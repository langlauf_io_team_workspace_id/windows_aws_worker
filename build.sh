git clone https://langlauf_io_js@bitbucket.org/langlauf_io_team_workspace_id/ansible_roles_windows_2019.git
vagrant destroy -f
vagrant up
vagrant halt

cd .vagrant/machines/default/vmware_desktop
cd $(find . -name '*-*-*-*-*' -type d)

# Clean (there are not .lck files if halted properly(?))
rm -rf *.vmx.lck

# Add
echo '{"provider": "vmware_desktop"}' > metadata.json  # See https://stackoverflow.com/a/38077488/4480139

cat << EOF > Vagrantfile
Vagrant.configure("2") do |config|
    config.vm.box = "langlauf_io/windows_2019_docker"
    config.vm.communicator = "winrm"

    config.winrm.username = "vagrant"
    config.winrm.password = "vagrant"

    config.vm.guest = :windows
    config.vm.base_mac = nil
    config.windows.halt_timeout = 15

    config.vm.network :forwarded_port, guest: 5985, host: 5958, id: "winrm", auto_correct: true
    config.vm.network :forwarded_port, guest: 3389, host: 3389, id: "rdp", auto_correct: true
    config.vm.network :forwarded_port, guest: 22, host: 2222, id: "ssh", auto_correct: true

end
EOF

# Compress (skipped for now, not sure how to find the main.vmdk)
#vmware-vdiskmanager -d /path/to/main.vmdk
#vmware-vdiskmanager -k /path/to/main.vmdk

# Package
rm windows_2019_docker.box || true
tar cvzf windows_2019_docker.box ./*

# Add .box file to vagrant boxes
PATH_TO_BOX="./windows_2019_docker.box"  # On Linux: PATH_TO_BOX=windows_2019_docker.box
MD5_OUTPUT=$(md5 windows_2019_docker.box)
MD5_HASH=$(echo $MD5_OUTPUT | cut -d ' ' -f4)

OLD_VERSION=`cat ../../../../../version.txt`
echo $OLD_VERSION

NEW_VERSION=$(python3 - "$OLD_VERSION" <<EOF
import sys
version = sys.argv[1]
#print(version)
base, _, minor = version.rpartition('.')
new_version_py = base + '.' + str(int(minor) + 1)
print(new_version_py)
sys.exit(new_version_py)
EOF)

cat << EOF > metadata.json
{
  "name": "langlauf_io/windows_2019_docker",
  "description": "Windows 2019 + docker + ssh + cygwin + ansible",
  "versions": [
    {
      "version": "$NEW_VERSION",
      "providers": [
        {
          "name": "vmware_desktop",
          "url": "$PATH_TO_BOX",
          "checksum_type": "md5",
          "checksum": "$MD5_HASH"
        }
      ]
    }
  ]
}
EOF

vagrant box add metadata.json --provider vmware_desktop --force

# Clean up
cd ../../../../../
echo $NEW_VERSION > version.txt
#vagrant destroy -f
#rm -rf .vagrant