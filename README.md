# Get everything we need
## Clone repos
- `git clone https://langlauf_io_js@bitbucket.org/langlauf_io_team_workspace_id/windows_2019_docker_base_box.git`

## Do it!
- `cd windows_2019_docker_base_box`
- `bash build.sh`

## Test
mkdir /tmp/delme
cd /tmp/delme
vagrant init langlauf_io/windows_2019_docker
vagrant up
echo "ls && sleep 2 && exit" | sshpass -p vagrant ssh -tt vagrant@127.0.0.1 -p $(vagrant port --guest 22)

